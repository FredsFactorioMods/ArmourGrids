data:extend(
{
  {
    type = "recipe",
    name = "personal-roboport-charging-equipment",
    enabled = false,
    energy_required = 10,
    ingredients =
    {
      {"personal-roboport-equipment", 2},
      {"processing-unit", 20}
    },
    result = "personal-roboport-charging-equipment"
  },
}
)

data:extend(
{
  {
    type = "roboport-equipment",
    name = "personal-roboport-charging-equipment",
    take_result = "personal-roboport-charging-equipment",
    sprite =
    {
      filename = "__Freds_Armour_Grids__/graphics/equipment/personal-roboport-charging-equipment.png",
      width = 64,
      height = 64,
      priority = "medium"
    },
    shape =
    {
      width = 2,
      height = 2,
      type = "full"
    },
    energy_source =
    {
      type = "electric",
      buffer_capacity = "35MJ",
      input_flow_limit = "3500KW",
      usage_priority = "secondary-input"
    },
    charging_energy = "1000kW",

    robot_limit = 0,
    construction_radius = 0,
    spawn_and_station_height = 0.8,
    charge_approach_distance = 5.2,

    recharging_animation =
    {
      filename = "__base__/graphics/entity/roboport/roboport-recharging.png",
      priority = "high",
      width = 37,
      height = 35,
      frame_count = 16,
      scale = 1.5,
      animation_speed = 0.5
    },
    recharging_light = {intensity = 0.4, size = 5},
    stationing_offset = {0, -0.6},
    charging_station_shift = {0, 0.5},
    charging_station_count = 8,
    charging_distance = 3.2,
    charging_threshold_distance = 5,
    categories = {"armor"}
  },
}
)