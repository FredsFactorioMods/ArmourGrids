data:extend({
  {
    type = "equipment-grid",
    name = "modified-small-equipment-grid",
    width = 8,
    height = 8,
    equipment_categories = {"armor"}
  },
  {
    type = "equipment-grid",
    name = "modified-equipment-grid",
    width = 10,
    height = 10,
    equipment_categories = {"armor"}
  },
  {
    type = "equipment-grid",
    name = "modified-equipment-grid-mk2",
    width = 14,
    height = 14,
    equipment_categories = {"armor"}
  },
  {
    type = "equipment-grid",
    name = "larger-equipment-grid",
    width = 20,
    height = 20,
    equipment_categories = {"armor"}
  },
  {
    type = "equipment-grid",
    name = "largest-equipment-grid",
    width = 28,
    height = 28,
    equipment_categories = {"armor"}
  }
})
