data:extend({
  {
    type = "armor",
    name = "modular-armor",
    icon = "__base__/graphics/icons/modular-armor.png",
    icon_size = 32,
    flags = {"goes-to-main-inventory"},
    resistances =
    {
      {
        type = "physical",
        decrease = 6,
        percent = 30
      },
      {
        type = "acid",
        decrease = 5,
        percent = 30
      },
      {
        type = "explosion",
        decrease = 30,
        percent = 35
      },
      {
        type = "fire",
        decrease = 0,
        percent = 40
      }
    },
    durability = 10000,
    subgroup = "armor",
    order = "c[modular-armor]",
    stack_size = 1,
    equipment_grid = "modified-small-equipment-grid",
    inventory_size_bonus = 20
  },
  {
    type = "armor",
    name = "power-armor",
    icon = "__base__/graphics/icons/power-armor.png",
	icon_size = 32,
    flags = {"goes-to-main-inventory"},
    resistances =
    {
      {
        type = "physical",
        decrease = 10,
        percent = 50
      },
      {
        type = "acid",
        decrease = 10,
        percent = 60
      },
      {
        type = "explosion",
        decrease = 10,
        percent = 60
      },
	  {
        type = "fire",
        decrease = 10,
        percent = 70
      }
    },
    durability = 150000,
    subgroup = "armor",
    order = "d[power-armor]",
    stack_size = 1,
    equipment_grid = "modified-equipment-grid",
    inventory_size_bonus = 40
  },	
  {
    type = "armor",
    name = "power-armor-mk2",
    icon = "__base__/graphics/icons/power-armor-mk2.png",
    icon_size = 32,
    flags = {"goes-to-main-inventory"},
    resistances =
    {
      {
        type = "physical",
        decrease = 10,
        percent = 40
      },
      {
        type = "acid",
        decrease = 10,
        percent = 40
      },
      {
        type = "explosion",
        decrease = 20,
        percent = 50
      },
      {
        type = "fire",
        decrease = 0,
        percent = 80
      }
    },
    durability = 20000,
    subgroup = "armor",
    order = "e[power-armor-mk2]",
    stack_size = 1,
    equipment_grid = "modified-equipment-grid-mk2",
    inventory_size_bonus = 60
  },
  {
    type = "armor",
    name = "power-armor-mk3",
    icon = "__Power Armor MK3__/graphics/icons/power-armor-mk3.png",  
    icon_size = 32,
    flags = {"goes-to-main-inventory"},
    resistances =
    {
      {
        type = "physical",
        decrease = 20,
        percent = 60
      },
      {
        type = "acid",
        decrease = 20,
        percent = 60
      },
      {
        type = "explosion",
        decrease = 30,
        percent = 60
      },
      {
        type = "fire",
        decrease = 0,
        percent = 100
      }
    },
    durability = 30000,
    subgroup = "armor",
    order = "f[power-armor-mk3]",
    stack_size = 1,
    equipment_grid = "larger-equipment-grid",
    inventory_size_bonus = 80
  },
  {
    type = "armor",
    name = "power-armor-mk4",
    icon = "__Power Armor MK3__/graphics/icons/power-armor-mk3.png",  
    icon_size = 32,
    flags = {"goes-to-main-inventory"},
    resistances =
    {
      {
        type = "physical",
        decrease = 30,
        percent = 60
      },
      {
        type = "acid",
        decrease = 30,
        percent = 60
      },
      {
        type = "explosion",
        decrease = 40,
        percent = 60
      },
      {
        type = "fire",
        decrease = 0,
        percent = 100
      }
    },
    durability = 40000,
    subgroup = "armor",
    order = "g[power-armor-mk4]",
    stack_size = 1,
    equipment_grid = "largest-equipment-grid",
    inventory_size_bonus = 100  
  },
  {
    type = "item",
    name = "personal-roboport-charging-equipment",
    icon_size = 32,
    icon = "__Freds_Armour_Grids__/graphics/icons/personal-roboport-charging-equipment.png",
    placed_as_equipment_result = "personal-roboport-charging-equipment",
    flags = {"goes-to-main-inventory"},
    subgroup = "equipment",
    order = "e-c",
    stack_size = 5
  }
})
