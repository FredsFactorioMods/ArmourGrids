:: Builds mod with versioning
::
@ECHO OFF

set /p MINOR=<version.txt
set VERSION=1.0.%MINOR%
set RELEASE_NAME=Freds_Armour_Grids_%VERSION%
set /A MINOR=MINOR+1
<NUL set /P MINOR=%MINOR%>version.txt

MD release\%RELEASE_NAME%
xcopy /s src release\%RELEASE_NAME%

powershell -Command "(gc release\%RELEASE_NAME%\info.json) -replace '__VERSION__', '%VERSION%' | Out-File release\%RELEASE_NAME%\info.json"

cd release
7z.exe a -tzip %RELEASE_NAME%.zip .\\%RELEASE_NAME%\\*

rd /s /q %RELEASE_NAME%

cd ..