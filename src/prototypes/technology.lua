data:extend(
{
  {
    type = "technology",
    name = "personal-roboport-charging-equipment",
    icon_size = 128,
    icon = "__base__/graphics/technology/personal-roboport-equipment.png",
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "personal-roboport-charging-equipment"
      }
    },
    prerequisites = { "personal-roboport-equipment-2" },
    unit =
    {
      count = 300,
      ingredients =
      {
        {"science-pack-1", 1},
        {"science-pack-2", 1},
        {"science-pack-3", 1},
        {"high-tech-science-pack", 1}
      },
      time = 45
    },
    order = "c-k-d-zz"
  },
}
)